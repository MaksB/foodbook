package com.epam.kitchen;

import com.epam.model.eat.Food;
import com.epam.modelenum.FoodMenu;

@FunctionalInterface
public interface FactoryKitchen {

	public Food cooking(FoodMenu foodMenu);
}
