package com.epam.kitchen;

import com.epam.kitchen.exception.IncorectMenuTypeException;
import com.epam.model.drink.Milk;
import com.epam.model.drink.Soda;
import com.epam.model.drink.Water;
import com.epam.model.eat.Food;
import com.epam.modelenum.DrinksMenu;
import com.epam.modelenum.FoodMenu;


public class FactoryDrinkMenu implements FactoryKitchen {

	@Override
	public Food cooking(FoodMenu foodMenu) {
		Food drink;
		DrinksMenu drinksMenu = (DrinksMenu) foodMenu;
		switch (drinksMenu) {
		case SODA:
			drink = new Soda();
			break;
		case MILK:
			drink = new Milk();
			break;
		case WATER:
			drink = new Water();
			break;
		default:
			throw new IncorectMenuTypeException();
		}
		return drink;
	}
}
