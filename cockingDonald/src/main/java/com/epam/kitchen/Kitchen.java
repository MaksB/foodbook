package com.epam.kitchen;

import com.epam.kitchen.exception.IncorectMenuTypeException;
import com.epam.model.eat.Food;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.OrderType;

public class Kitchen {
	private FactoryKitchen factoryKitchen;

	public Food createEat(OrderType type, FoodMenu foodMenu) {
		switch (type) {
		case VEGETARIAN:
			factoryKitchen = new FactroryCookingVegMenu();
			break;
		case DRINKS:
			factoryKitchen = new FactoryDrinkMenu();
			break;
		case STANDARD:
			factoryKitchen = new FactoryCookingEat();
			break;
		default:
			throw new IncorectMenuTypeException();

		}

		return factoryKitchen.cooking(foodMenu);
	}
}
