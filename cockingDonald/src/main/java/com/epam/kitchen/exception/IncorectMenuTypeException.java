package com.epam.kitchen.exception;

public class IncorectMenuTypeException extends RuntimeException {

	public IncorectMenuTypeException() {
	}

	public IncorectMenuTypeException(Throwable cause) {
		super(cause);
	}

	public IncorectMenuTypeException(String message) {
		super(message);
	}
	public IncorectMenuTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	@Override
	public String getMessage() {
		return "Ivalide type menu";
	}
}
