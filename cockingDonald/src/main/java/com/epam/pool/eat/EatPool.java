package com.epam.pool.eat;

import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.epam.model.eat.Food;
import com.epam.modelenum.FoodMenu;
import com.epam.modelenum.OrderType;
import com.epam.kitchen.Kitchen;

public class EatPool {

	private Map<OrderType, Map<FoodMenu, List<Food>>> pool =Collections.synchronizedMap( new EnumMap(OrderType.class));
	private Kitchen kitchen;
	private static volatile EatPool eatPool;

	private EatPool() {
		kitchen = new Kitchen();
		initializationFoodPool();
	}

	public static EatPool getInstance() {
		if (eatPool == null) {
			synchronized (EatPool.class) {		
					eatPool =  new EatPool();

			}
		}
		return eatPool;
	}

	public Food getEat(OrderType typeEat, FoodMenu foodMenu){
		List<Food> foodList = pool.get(typeEat).get(foodMenu);
		updatePool(foodList, typeEat, foodMenu);
		return foodList.remove(0);
	}
	
	public void saveFood(Food food, OrderType typeEat, FoodMenu foodMenu){
		List<Food> foodList = pool.get(typeEat).get(foodMenu);
		foodList.add(food);
	}

	private void updatePool(List<Food> eaList, OrderType typeEat, FoodMenu foodMenu) {
		if (eaList.size() <= 3) {
			for (int i = 0; i < 3; i++) {
				eaList.add(kitchen.createEat(typeEat, foodMenu));
			}

		}
	}

	private void initializationFoodPool() {
		for (OrderType orderType : OrderType.values()) {
			pool.put(orderType, new HashMap<>());
			for (FoodMenu foodMenu : orderType.getFood()) {
				List<Food> foodList = new LinkedList<>();
				for (int i = 0; i < 5; i++) {
					foodList.add(kitchen.createEat(orderType, foodMenu));
				}
				pool.get(orderType).put(foodMenu, foodList);
			}
		}
	}
}
