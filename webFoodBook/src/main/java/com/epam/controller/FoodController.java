package com.epam.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.flyweight.order.FlyiweightOrders;
import com.epam.modelenum.OrderType;
import com.epam.service.FacadeHistoryService;
import com.epam.service.facade.FacadeHistoryServiceImpl;

@Controller
public class FoodController {
	private FacadeHistoryService historService;

	@PostConstruct
	private void init() {
		historService = new FacadeHistoryServiceImpl();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("index");
		for (OrderType orderType : OrderType.values()) {
			FlyiweightOrders.getOrder(orderType);
		}
		model.addObject("mapOrders", FlyiweightOrders.getOrderStorage());
		return model;
	}

	@RequestMapping(value = "/rate", method = RequestMethod.POST)
	public ModelAndView rate(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView model = new ModelAndView("rate");
		model.addObject("orderRate", FlyiweightOrders.getOrder(OrderType.valueOf(request.getParameter("order"))));
		return model;
	}

	@RequestMapping(value = "/createRate", method = RequestMethod.POST)
	public String createRate(HttpServletRequest request) {
		historService.addHistoryObject(OrderType.valueOf(request.getParameter("orderRate")),
				request.getParameter("taste"), request.getParameter("overallLook"), request.getParameter("price"),
				request.getParameter("cookingTime"), request.getParameter("comment"));
		return "redirect:/";

	}
	
	@RequestMapping(value = "/calculateNewPrice", method = RequestMethod.GET)
	public String calculateNewPrice(HttpServletRequest request){
		String orderType = request.getParameter("orderType");
		String type = request.getParameter("typeCalculate");
		historService.calculateNewRating(orderType, type);
		return "redirect:/";
	}

}
