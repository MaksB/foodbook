package com.epam.controller;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.service.FacadeHistoryService;
import com.epam.service.facade.FacadeHistoryServiceImpl;

@Controller
public class HistroyController {

	private FacadeHistoryService historService;

	@PostConstruct
	private void init() {
		historService = new FacadeHistoryServiceImpl();
	}

	@RequestMapping(value = "/addComment", method = RequestMethod.POST)
	public String addComment(HttpServletRequest request) {
		Long commentId = null;
		if(!request.getParameter("commentId").isEmpty()){
			commentId = Long.parseLong(request.getParameter("commentId")); 
		}
		historService.addComment(request.getParameter("comment"),
				Integer.parseInt(request.getParameter("idOrder")),
						commentId);
		return "redirect:/history";
	}

	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public ModelAndView getHistory() {
		ModelAndView model = new ModelAndView("history");
		model.addObject("allHistory", historService.getAllOrders());
		return model;
	}
}
