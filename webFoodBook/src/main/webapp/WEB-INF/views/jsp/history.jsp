<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>History</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="/webFoodBook/history">FoodBook
				History</a> <a class="navbar-brand" href="/webFoodBook/">FoodBook</a>
		</div>
	</div>
</nav>

<div class="jumbotron">
	<div class="container">
		<h1>${title}</h1>
		${orderR}
		<c:forEach items="${allHistory}" var="entry" varStatus="step">
			<div class="container">
				${entry.key}
				<c:if test="${entry.value != null}">

					<c:forEach items="${entry.value}" var="comment">
						<div class="commentForm">${comment.dispalyComment()}<p>comment id ${comment.getId()}</p>
						<p>root comment id ${comment.getRootId()}</p></div>
						<form action="/webFoodBook/addComment" id="subcommentForm"
							method="post">
							<input type="hidden" name="idOrder" value="${(step.count-1)}">
							<input type="hidden" name="commentId" value="${comment.getId()}">
							<div>
								<textarea class="commentForm" name="comment" form="subcommentForm"
									placeholder="Enter subcomment here..."></textarea>
							</div>
							<input class="btn btn-primary" type="submit" value="Subcomment">
						</form>
					</c:forEach>
				</c:if>
				<form action="/webFoodBook/addComment" id="commentForm"
					method="post">
					<input type="hidden" name="idOrder" value="${(step.count-1)}">
					<input type="hidden" name="commentId" value="1">
					<div>
						<textarea class="commentForm" name="comment" form="commentForm"
							placeholder="Enter comment here..."></textarea>
					</div>
					<input class="btn btn-primary" type="submit" value="Comment">
				</form>

			</div>
		</c:forEach>

	</div>
</div>


<spring:url value="/resources/core/css/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</body>
</html>