<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Orderers</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="/webFoodBook/history">FoodBook
				History</a> <a class="navbar-brand" href="/webFoodBook/">FoodBook</a>
		</div>
	</div>
</nav>

<div class="jumbotron">
	<div class="container">
		<h1>${title}</h1>
		${orderR}
		<c:forEach items="${mapOrders}" var="entry">
			<div class="container">

				<form action="/webFoodBook/rate" method="post">
					Order type: ${entry.key} order: ${entry.value.getPrice()} :
					${entry.value.getOrder().getDate()} :
					${entry.value.getOrder().getFoodList()} <input type="hidden"
						name="order" value="${entry.key}"> <input
						class="btn btn-primary" type="submit" value="Rate">
				</form>
				<h4>Calculate new price</h4>
				<a class="btn btn-primary" href="/webFoodBook/calculateNewPrice?orderType=${entry.key}&&typeCalculate=overallLook">Overall Look</a> 
				<a class="btn btn-primary" href="/webFoodBook/calculateNewPrice?orderType=${entry.key}&&typeCalculate=cookingTime">Cooking Time</a> 
				<a class="btn btn-primary" href="/webFoodBook/calculateNewPrice?orderType=${entry.key}&&typeCalculate=taste">Taste</a> 
				<a class="btn btn-primary" href="/webFoodBook/calculateNewPrice?orderType=${entry.key}&&typeCalculate=price">Price</a> 
				<a class="btn btn-primary" href="/webFoodBook/calculateNewPrice?orderType=${entry.key}&&typeCalculate=standart">Standart</a> 
			</div>
		</c:forEach>

	</div>
</div>

<spring:url value="/resources/core/css/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</body>
</html>