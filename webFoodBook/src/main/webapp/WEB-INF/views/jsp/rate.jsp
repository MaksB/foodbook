<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<head>
<title>Rate</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
				<a class="navbar-brand" href="/webFoodBook/history">FoodBook History</a>
		</div>
	</div>
</nav>

<div class="jumbotron">
	<div class="container">
		<h1>${title}</h1>
		<div class="container">
			<form modelAttribute="OrderRate" action="/webFoodBook/createRate"
				id="rateForm" method="post">
				Order type: ${orderRate.getOrder().getOrderType()} 
				order price:${orderRate.getPrice()} : date ${orderRate.getOrder().getDate()} :
				Food list ${orderRate.getOrder().getFoodList()}
				<div>
					<input type="hidden" name="orderRate"
						value="${orderRate.getOrder().getOrderType()}"> Taste :<input
						type="text" name="taste" pattern="[1-9]|10" title="Enter 1-10">
				</div>
				<div>
					Cooking time :<input type="text" name="cookingTime"
						pattern="[1-9]|10" title="Enter 1-10">
				</div>
				<div>
					Overal look: <input type="text" name="overallLook"
						pattern="[1-9]|10" title="Enter 1-10">
				</div>
				<div>
					Price : <input type="text" name="price" pattern="[1-9]|10"
						title="Enter 1-10">
				</div>

				<div>
					<textarea name="comment" form="rateForm"
						placeholder="Enter comment here..."></textarea>
				</div>
				<input class="btn btn-primary" type="submit" value="Rate">
			</form>

		</div>

	</div>
</div>

<div class="container"></div>

<spring:url value="/resources/core/css/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</body>
</html>