package com.epam.flyweight.order;

import java.util.EnumMap;
import java.util.Map;

import com.epam.abstractfactory.AbstractFactoryServiceImpl;
import com.epam.model.order.Order;
import com.epam.model.order.ResultOrder;
import com.epam.modelenum.DrinksMenu;
import com.epam.modelenum.OrderType;
import com.epam.modelenum.StandartMenu;
import com.epam.modelenum.VegetarianMenu;

public class FlyiweightOrders {

	private static final Map<OrderType, ResultOrder> orderStorage = new EnumMap<>(OrderType.class);
	private static	AbstractFactoryServiceImpl abstractFactoryServiceImpl = new AbstractFactoryServiceImpl();

	public static ResultOrder getOrder(OrderType orderType){
		if(orderStorage.get(orderType) == null){
			if(orderType == OrderType.DRINKS){
				Order drinkOrder = new Order();
				drinkOrder.setOrderType(OrderType.DRINKS);
				drinkOrder.getFoodList().add(DrinksMenu.MILK);
				drinkOrder.getFoodList().add(DrinksMenu.SODA);
				drinkOrder.getFoodList().add(DrinksMenu.WATER);
				orderStorage.put(OrderType.DRINKS,
						abstractFactoryServiceImpl.getDrinkService().getOrderService().getOrder(drinkOrder));
			}else if(orderType == OrderType.STANDARD){
				Order standartOrder = new Order();
				standartOrder.setOrderType(OrderType.STANDARD);
				standartOrder.getFoodList().add(StandartMenu.BIGMUCK);
				standartOrder.getFoodList().add(StandartMenu.BURGER);
				standartOrder.getFoodList().add(StandartMenu.ICE_CREAM);
				orderStorage.put(OrderType.STANDARD,
						abstractFactoryServiceImpl.getStandartService().getOrderService().getOrder(standartOrder));
			}else if(orderType == OrderType.VEGETARIAN){
				Order vegetarianOrder = new Order();
				vegetarianOrder.setOrderType(OrderType.VEGETARIAN);
				vegetarianOrder.getFoodList().add(VegetarianMenu.ICE_CREAM);
				vegetarianOrder.getFoodList().add(VegetarianMenu.VEGA_BIGMUCK);
				vegetarianOrder.getFoodList().add(VegetarianMenu.VEGA_BURGER);
				orderStorage.put(OrderType.VEGETARIAN,
						abstractFactoryServiceImpl.getVegetarianService().getOrderService().getOrder(vegetarianOrder));
			}
		}
		
		return orderStorage.get(orderType);
	}

	public static Map<OrderType, ResultOrder> getOrderStorage() {
		return orderStorage;
	}

}
