package com.epam.abstractfactory;

public interface AbstractFactoryService {
	
	public ServiceFactory getVegetarianService();
	public ServiceFactory getStandartService();
	public ServiceFactory getDrinkService();
}
