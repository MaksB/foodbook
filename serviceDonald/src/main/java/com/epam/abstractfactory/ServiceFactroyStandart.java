package com.epam.abstractfactory;

import com.epam.service.OrderService;
import com.epam.service.standart.StandartOrderService;

public class ServiceFactroyStandart implements ServiceFactory{

	@Override
	public OrderService getOrderService() {
		return new StandartOrderService();
	}
}
