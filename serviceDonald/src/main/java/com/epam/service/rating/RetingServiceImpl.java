package com.epam.service.rating;

import java.util.List;

import com.epam.model.order.AdapterOrder;
import com.epam.model.order.Rating;
import com.epam.model.order.ResultOrder;
import com.epam.modelenum.OrderType;
import com.epam.service.HistoryService;
import com.epam.service.RatingService;
import com.epam.service.history.HistoryServiceImpl;

public class RetingServiceImpl implements RatingService {

	HistoryService historyService = new HistoryServiceImpl();
	
	@Override
	public Rating calculateReting(ResultOrder resultOrder) {
		Rating rating = modifyCost(resultOrder.getOrder().getOrderType());
		int calculateRating = (rating.getCookingTime() + rating.getOveralLook() + rating.getPrice() + rating.getTaste())
				/ 4;
		rating.setRating(calculateRating);
		return rating;
	}


	public Rating modifyCost(OrderType orderType) {
		List<AdapterOrder> addapterOrders = historyService.getByType(orderType);
		return getMidleRating(addapterOrders);
	}

	private Rating getMidleRating(List<AdapterOrder> addapterOrders) {
		double taste = 0;
		double cookingTime = 0;
		double overalLook = 0;
		double price = 0;
		for (AdapterOrder adapterOrder : addapterOrders) {
			taste += adapterOrder.getRating().getTaste();
			cookingTime += adapterOrder.getRating().getCookingTime();
			overalLook += adapterOrder.getRating().getOveralLook();
			price += adapterOrder.getRating().getPrice();
		}
		taste = taste / addapterOrders.size();
		cookingTime = cookingTime / addapterOrders.size();
		overalLook = overalLook / addapterOrders.size();
		price = price / addapterOrders.size();
		Rating mideleRating = new Rating();
		mideleRating.setCookingTime((int) cookingTime);
		mideleRating.setOveralLook((int) overalLook);
		mideleRating.setPrice((int) price);
		mideleRating.setTaste((int) taste);

		return mideleRating;
	}

	
}
