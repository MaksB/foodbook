package com.epam.service.rating;

import com.epam.model.order.Rating;
import com.epam.model.order.ResultOrder;
import com.epam.modelenum.OrderType;
import com.epam.service.RatingService;

public class OverallLookDecoratorRetingService implements RatingService {

	private RatingService ratingService;
	
	public OverallLookDecoratorRetingService(RatingService ratingService) {
		this.ratingService = ratingService;
	}
	
	@Override
	public Rating calculateReting(ResultOrder resultOrder) {
		Rating rating = modifyCost(resultOrder.getOrder().getOrderType());
		rating.setRating(rating.getOveralLook());
		return rating;
	}

	@Override
	public Rating modifyCost(OrderType orderType) {
		return ratingService.modifyCost(orderType);
	}

}
