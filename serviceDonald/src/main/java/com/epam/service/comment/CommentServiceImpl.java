package com.epam.service.comment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.epam.composite.Comment;
import com.epam.composite.CommentInterface;
import com.epam.composite.ProxyComment;
import com.epam.service.CommentService;

public class CommentServiceImpl implements CommentService {

	@Override
	public List<CommentInterface> getProxyComments(Comment comment) {
		List<CommentInterface> commentsProxy = new ArrayList<>();
		ProxyComment proxyComment = new ProxyComment(comment);
		commentsProxy.add(proxyComment);
		parseGraph(proxyComment.getSubComment(), commentsProxy);
		return commentsProxy;
	}
	
	private List<CommentInterface> parseGraph(List<CommentInterface> comments, List<CommentInterface> allComments ){
		allComments.addAll(comments);
		for (CommentInterface commentInterface : comments) {
			if(!commentInterface.getSubComment().isEmpty()){
				parseGraph(commentInterface.getSubComment(), allComments);
			}
		}
		return allComments;
	}

	@Override
	public Comment addCooment(Comment comment, String msg, Long commentId) {
		if (commentId == null) {
			comment = createComment(msg, 1L, 1L);
		} else {
			if (comment.getId() == commentId) {
				comment.getSubComment().add(createComment(msg, comment.getRootId() + 1, commentId));
				comment.setRootId(comment.getRootId() + 1);
			} else {
				graphSearch(comment.getSubComment(), comment, commentId, msg);
			}
		}

		return comment;
	}

	private void graphSearch(List<CommentInterface> list, Comment rootComment, Long commentId, String msg) {
		for (CommentInterface comment : list) {
			if (comment.getId() == commentId) {
				comment.getSubComment().add(createComment(msg, rootComment.getRootId() + 1, commentId));
				rootComment.setRootId(rootComment.getRootId() + 1);
			} else if (!comment.getSubComment().isEmpty()) {
				graphSearch(comment.getSubComment(), rootComment, commentId, msg);
			}
		}
	}

	private Comment createComment(String msg, Long id, Long rootId) {
		Comment newComment = new Comment();
		newComment.setId(id);
		newComment.setRootId(rootId);
		newComment.setDateComment(new Date());
		newComment.setMsg(msg);
		return newComment;
	}
}
