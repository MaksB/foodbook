package com.epam.service;

import java.util.List;
import java.util.Map;

import com.epam.composite.CommentInterface;
import com.epam.model.order.AdapterOrder;
import com.epam.modelenum.OrderType;

public interface FacadeHistoryService {

	public Map<AdapterOrder, List<CommentInterface>> getAllOrders();
	public void addHistoryObject(OrderType orderType, String taste, String overallLook, String price, String cookingTime, String comment);
	public void addComment(String commentMessage, Integer id, Long commentId);
	public void calculateNewRating(String orderType, String type);
}
