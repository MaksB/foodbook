package com.epam.service.history;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.epam.model.order.AdapterOrder;
import com.epam.modelenum.OrderType;
import com.epam.service.HistoryService;

public class HistoryServiceImpl implements HistoryService {

	private static List<AdapterOrder> historyStorage = new ArrayList<>();

	@Override
	public void addNewItem(AdapterOrder adapterOrder) {
		if (!historyStorage.contains(adapterOrder)) {
			historyStorage.add(adapterOrder);
		}

	}

	@Override
	public List<AdapterOrder> getByType(OrderType orderType) {
		return historyStorage.stream().filter(o -> o.getResultOrder().getOrder().getOrderType() == orderType)
				.collect(Collectors.toList());
	}

	@Override
	public List<AdapterOrder> getAll() {
		return historyStorage;
	}

	@Override
	public int getMidleRating(OrderType orderType) {
		int ratingSum = 0;
		List<AdapterOrder> listOrders = historyStorage.stream()
				.filter(o -> o.getResultOrder().getOrder().getOrderType() == orderType).collect(Collectors.toList());
		for (AdapterOrder adapterOrder : listOrders) {
			ratingSum += adapterOrder.getRating().getRating();
		}
		ratingSum = listOrders.size();
		return ratingSum;
	}
	
	@Override
	public AdapterOrder getById(Integer id) {
		return historyStorage.get(id);
	}

}
