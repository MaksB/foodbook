package com.epam.service.facade;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.epam.composite.Comment;
import com.epam.composite.CommentInterface;
import com.epam.flyweight.order.FlyiweightOrders;
import com.epam.model.order.AdapterOrder;
import com.epam.model.order.Rating;
import com.epam.model.order.ResultOrder;
import com.epam.modelenum.OrderType;
import com.epam.service.CommentService;
import com.epam.service.FacadeHistoryService;
import com.epam.service.HistoryService;
import com.epam.service.RatingService;
import com.epam.service.comment.CommentServiceImpl;
import com.epam.service.history.HistoryServiceImpl;
import com.epam.service.rating.CookingTimeDecoratorServiceImpl;
import com.epam.service.rating.OverallLookDecoratorRetingService;
import com.epam.service.rating.PriceDecoratorRatingService;
import com.epam.service.rating.RetingServiceImpl;
import com.epam.service.rating.TasteDecoratorRatingServiceImpl;

public class FacadeHistoryServiceImpl implements FacadeHistoryService {

	private HistoryService historyService;
	private CommentService commentService;
	private RatingService ratingService;

	public FacadeHistoryServiceImpl() {
		historyService = new HistoryServiceImpl();
		commentService = new CommentServiceImpl();
		ratingService = new RetingServiceImpl();
	}

	public void addHistoryObject(com.epam.modelenum.OrderType orderType, String taste, String overallLook, String price,
			String cookingTime, String commentMessage) {
		Rating rating = new Rating();
		Comment comment = null;
		rating.setCookingTime(parse(cookingTime));
		rating.setOveralLook(parse(overallLook));
		rating.setTaste(parse(taste));
		rating.setPrice(parse(price));

		if (!commentMessage.isEmpty()) {
			comment = new Comment();
			comment.setId(1L);
			comment.setMsg(commentMessage);
			comment.setDateComment(new Date());
		}

		ResultOrder resultOrder = FlyiweightOrders.getOrder(orderType);
		AdapterOrder adapterOrder = new AdapterOrder();
		adapterOrder.setResultOrder(resultOrder);
		adapterOrder.setRating(rating);
		adapterOrder.setComment(comment);
		historyService.addNewItem(adapterOrder);
	}

	@Override
	public Map<AdapterOrder, List<CommentInterface>> getAllOrders() {
		Map<AdapterOrder, List<CommentInterface>> allorders = new HashMap<>();
		List<AdapterOrder> listorders = historyService.getAll();
		for (AdapterOrder adapterOrder : listorders) {
			allorders.put(adapterOrder, commentService.getProxyComments((Comment) adapterOrder.getComment()));
		}
		return allorders;
	}

	@Override
	public void addComment(String commentMessage, Integer id, Long commentId) {
		AdapterOrder adapterOrder = historyService.getById(id);
		Comment newComment = commentService.addCooment((Comment) adapterOrder.getComment(), commentMessage, commentId);
		adapterOrder.setComment(newComment);
	}

	public void calculateNewRating(String orderType, String type){
		ResultOrder resultOrder = FlyiweightOrders.getOrder(OrderType.valueOf(orderType));
		Rating rating = null;
		switch (type) {
		case "taste":
			RatingService tasteRatingService = new TasteDecoratorRatingServiceImpl(ratingService);
			rating = tasteRatingService.calculateReting(resultOrder);
			resultOrder.setPrice(calculateRate(rating.getRating(), resultOrder.getPrice()));
			break;
		case "cookingTime":
			RatingService cookintTimeRatingService = new CookingTimeDecoratorServiceImpl(ratingService);
			rating = cookintTimeRatingService.calculateReting(resultOrder);
			resultOrder.setPrice(calculateRate(rating.getRating(), resultOrder.getPrice()));
			break;
		case "overallLook":
			RatingService overallLookTimeRatingService = new OverallLookDecoratorRetingService(ratingService);
			rating = overallLookTimeRatingService.calculateReting(resultOrder);
			resultOrder.setPrice(calculateRate(rating.getRating(), resultOrder.getPrice()));
			break;
		case "price":
			RatingService priceRatingService = new PriceDecoratorRatingService(ratingService);
			rating = priceRatingService.calculateReting(resultOrder);
			resultOrder.setPrice(calculateRate(rating.getRating(), resultOrder.getPrice()));
			break;
		default:
			rating = ratingService.calculateReting(resultOrder);
			resultOrder.setPrice(calculateRate(rating.getRating(), resultOrder.getPrice()));
			break;
		}
	}

	private double calculateRate(int rate, double price) {
		if (rate == 5) {
			price += (price / 100) * 10;
		} else if (rate <= 3 / 10) {
			price -= (price / 100) * 20;
		} else if (rate >= 8 / 10) {
			price += (price / 100) * 30;
		}
		return price;
	}

	private int parse(String stringNumber) {
		return Integer.parseInt(stringNumber);
	}
}
