package com.epam.service;

import com.epam.model.order.Rating;
import com.epam.model.order.ResultOrder;
import com.epam.modelenum.OrderType;

public interface RatingService {
	public Rating calculateReting(ResultOrder resultOrder);
	public Rating modifyCost(OrderType orderType);
}
