package com.epam.service;


import java.util.List;

import com.epam.composite.Comment;
import com.epam.composite.CommentInterface;

public interface CommentService {

	public List<CommentInterface> getProxyComments(Comment comment);
	public Comment addCooment(Comment comment, String msg, Long comentId);
}
