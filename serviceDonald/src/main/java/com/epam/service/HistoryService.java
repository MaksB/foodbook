package com.epam.service;

import java.util.List;

import com.epam.model.order.AdapterOrder;
import com.epam.modelenum.OrderType;

public interface HistoryService {

	public void addNewItem(AdapterOrder addapterOrder);
	public List<AdapterOrder> getByType(OrderType orderType);
	public List<AdapterOrder> getAll();
	public int getMidleRating(OrderType orderType);
	public AdapterOrder getById(Integer id);
}
