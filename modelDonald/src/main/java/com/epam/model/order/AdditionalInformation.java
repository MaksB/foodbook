package com.epam.model.order;

import com.epam.composite.CommentInterface;

public class AdditionalInformation {

	private CommentInterface comment;
	private Rating rating;

	public CommentInterface getComment() {
		return comment;
	}

	public void setComment(CommentInterface comment) {
		this.comment = comment;
	}

	public Rating getRating() {
		return rating;
	}

	public void setRating(Rating rating) {
		this.rating = rating;
	}

}
