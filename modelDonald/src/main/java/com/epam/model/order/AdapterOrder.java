package com.epam.model.order;

public class AdapterOrder extends AdditionalInformation{

	public AdapterOrder() {
	}
	private ResultOrder resultOrder;

	public ResultOrder getResultOrder() {
		return resultOrder;
	}

	public void setResultOrder(ResultOrder resultOrder) {
		this.resultOrder = resultOrder;
	}

	@Override
	public String toString() {
		return "AdapterOrder [resultOrder=" + resultOrder + ", getRating()="
				+ getRating() + "]";
	}
	
	
}
