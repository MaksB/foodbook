package com.epam.model.order;

public class Rating {

	private int taste;
	private int cookingTime;
	private int overalLook;
	private int price;
	private int rating;
	public int getTaste() {
		return taste;
	}
	public void setTaste(int taste) {
		this.taste = taste;
	}
	public int getCookingTime() {
		return cookingTime;
	}
	public void setCookingTime(int cookingtime) {
		this.cookingTime = cookingtime;
	}
	public int getOveralLook() {
		return overalLook;
	}
	public void setOveralLook(int overalLook) {
		this.overalLook = overalLook;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "Rating [taste=" + taste + ", cookingTime=" + cookingTime + ", overalLook=" + overalLook + ", price="
				+ price + ", rating=" + rating + "]";
	}
	
}
