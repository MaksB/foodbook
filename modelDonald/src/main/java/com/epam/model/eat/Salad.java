package com.epam.model.eat;

public class Salad extends Food{
	
	public Salad() {
		name = "Salad";
		COST = 1;
	}
	
	@Override
	public PrototypeFood cloneFood() {
		Salad salad = new Salad();
		salad.setCost(COST);
		salad.setName(name);
		return salad;
	}

	@Override
	public String toString() {
		return "Salad [cost=" + COST + ", name=" + name + "]";
	}
}
