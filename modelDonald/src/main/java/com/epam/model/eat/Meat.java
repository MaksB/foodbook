package com.epam.model.eat;

public class Meat extends Food{
	private boolean dietary;

	public Meat() {
		dietary = false;
		name = "Beef";
		COST = 3;
	}
	
	public boolean isDietary() {
		return dietary;
	}

	public void setDietary(boolean dietary) {
		this.dietary = dietary;
	} 
	
	@Override
	public PrototypeFood cloneFood() {
		Meat meat = new Meat();
		meat.setCost(COST);
		meat.setDietary(dietary);
		meat.setName(name);
		return meat;
	}

	@Override
	public String toString() {
		return "Meat [dietary=" + dietary + ", cost=" + COST + ", name=" + name + "]";
	}

}
