package com.epam.model.eat;

public class Soy extends Food {

	public Soy() {
		name = "Soy";
		COST = 1;
	}

	@Override
	public PrototypeFood cloneFood() {
		Soy soy = new Soy();
		soy.setCost(COST);
		soy.setName(name);
		return soy;
	}

	@Override
	public String toString() {
		return "Soy [cost=" + COST + ", name=" + name + "]";
	}
}
