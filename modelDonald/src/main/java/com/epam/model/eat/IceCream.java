package com.epam.model.eat;

public class IceCream extends Food{

	private boolean chocolate;

	public IceCream() {
		name = "Plombir";
		COST = 4;
		chocolate = false;
	}
	
	public boolean isChocolate() {
		return chocolate;
	}

	public void setChocolate(boolean chocolate) {
		this.chocolate = chocolate;
	}
	
	@Override
	public PrototypeFood cloneFood() {
		IceCream iceCream = new IceCream();
		iceCream.setChocolate(chocolate);
		iceCream.setCost(COST);
		iceCream.setName(name);
		return iceCream;
	}

	@Override
	public String toString() {
		return "IceCream [chocolate=" + chocolate + ", cost=" + COST + ", name=" + name + "]";
	}
	
	
}
