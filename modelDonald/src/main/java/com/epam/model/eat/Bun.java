package com.epam.model.eat;

public class Bun extends Food {

	private String flour;

	public Bun() {
		flour = "White flour";
		COST = 5;
		name = "Bun";
	}

	public String getFlour() {
		return flour;
	}

	public void setFlour(String flour) {
		this.flour = flour;
	}

	@Override
	public PrototypeFood cloneFood() {
		Bun bun = new Bun();
		bun.setCost(COST);
		bun.setFlour(flour);
		bun.setName(name);
		return bun;
	}

	@Override
	public String toString() {
		return "Bun [flour=" + flour + ", cost=" + COST + ", name=" + name + "]";
	}
}
