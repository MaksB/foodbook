package com.epam.model.eat;

import com.epam.model.eat.applications.Ketchup;
import com.epam.model.eat.applications.Mustard;
import com.epam.model.eat.applications.Salt;

public class Burger extends Food {

	private Bun bun;
	private Salad salad;
	private Chees chees;
	private Meat meat;
	private Soy soy;
	private Ketchup ketchup;
	private Mustard mustard;
	private Salt salt;

	private Burger() {
	}

	public Burger(BurgerBuilder burgerBuilder) {
		bun = burgerBuilder.bun;
		salad = burgerBuilder.salad;
		chees = burgerBuilder.chees;
		meat = burgerBuilder.meat;
		soy = burgerBuilder.soy;
		ketchup = burgerBuilder.ketchup;
		mustard = burgerBuilder.mustard;
		salt = burgerBuilder.salt;
		COST = burgerBuilder.cost;
		name = "Burger";
		сalculateCost();

	}

	public static class BurgerBuilder {
		private double cost;
		private final Bun bun;
		private Salad salad;
		private Chees chees;
		private Meat meat;
		private Soy soy;
		private Ketchup ketchup;
		private Mustard mustard;
		private Salt salt;

		public BurgerBuilder(Bun bun) {
			this.bun = bun;
		}

		public BurgerBuilder addChees(Chees chees) {
			this.chees = chees;
			return this;
		}

		public BurgerBuilder addSalad(Salad salad) {
			this.salad = salad;
			return this;
		}

		public BurgerBuilder addMeat(Meat meat) {
			this.meat = meat;
			return this;
		}

		public BurgerBuilder addSoy(Soy soy) {
			this.soy = soy;
			return this;
		}

		public BurgerBuilder addKetchup(Ketchup ketchup) {
			this.ketchup = ketchup;
			return this;
		}

		public BurgerBuilder addMustard(Mustard mustard) {
			this.mustard = mustard;
			return this;
		}

		public BurgerBuilder addSalt(Salt salt) {
			this.salt = salt;
			return this;
		}

		public Burger crate() {
			return new Burger(this);
		}

	}

	public Bun getBread() {
		return bun;
	}

	public Salad getSalad() {
		return salad;
	}

	public Chees getChees() {
		return chees;
	}

	public Meat getMeat() {
		return meat;
	}

	public Soy getSoy() {
		return soy;
	}

	public Ketchup getKetchup() {
		return ketchup;
	}

	public Mustard getMustard() {
		return mustard;
	}

	public Salt getSalt() {
		return salt;
	}

	public void setBread(Bun bread) {
		this.bun = bread;
	}

	public void setSalad(Salad salad) {
		this.salad = salad;
	}

	public void setChees(Chees chees) {
		this.chees = chees;
	}

	public void setMeat(Meat meat) {
		this.meat = meat;
	}

	public void setSoy(Soy soy) {
		this.soy = soy;
	}

	public void setKetchup(Ketchup ketchup) {
		this.ketchup = ketchup;
	}

	public void setMustard(Mustard mustard) {
		this.mustard = mustard;
	}

	public void setSalt(Salt salt) {
		this.salt = salt;
	}
	private void сalculateCost(){
		COST += bun.getCost();
		if (chees != null) {
			COST +=chees.getCost();
		}
		if (meat != null) {
			COST += meat.getCost();
		}
		if (soy != null) {
			COST += soy.getCost();
		}
		if (ketchup != null) {
			COST += ketchup.getCost();
		}
		if (mustard != null) {
			COST += mustard.getCost();
		}
		if (salad != null) {
			COST += salad.getCost();
		}
		if (salt != null) {
			COST += salt.getCost();
		}
	}


	

	@Override
	public String toString() {
		return "Burger [bun=" + bun + ", salad=" + salad + ", chees=" + chees + ", meat=" + meat + ", soy=" + soy
				+ ", ketchup=" + ketchup + ", mustard=" + mustard + ", salt=" + salt + ", COST=" + COST + ", name="
				+ name + "]";
	}

	@Override
	public PrototypeFood cloneFood() {
		Burger burger = new Burger();
		burger.setName(name);
		burger.setCost(COST);
		burger.setBread((Bun) bun.cloneFood());
		if (chees != null) {
			burger.setChees((Chees) chees.cloneFood());
		}
		if (ketchup != null) {
			burger.setKetchup( (Ketchup) ketchup.cloneFood());
		}
		if (meat != null) {
			burger.setMeat((Meat) meat.cloneFood());
		}
		if (mustard != null) {
			burger.setMustard((Mustard) mustard.cloneFood());
		}
		if (salad != null) {
			burger.setSalad((Salad) salad.cloneFood());
		}
		if (soy != null) {
			burger.setSoy((Soy) soy.cloneFood());
		}

		return burger;
	}

}
