package com.epam.model.eat.applications;

import com.epam.model.eat.Food;
import com.epam.model.eat.PrototypeFood;

public class Ketchup extends Food{

	public Ketchup() {
		COST = 1;
		name = "Ketchup";
	}
	
	@Override
	public PrototypeFood cloneFood() {
		Ketchup ketchup = new Ketchup();
		ketchup.setCost(COST);
		ketchup.setName(name);
		return ketchup;
	}

	@Override
	public String toString() {
		return "Ketchup [cost=" + COST + ", name=" + name + "]";
	}
	
}
