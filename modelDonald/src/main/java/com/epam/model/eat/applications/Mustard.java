package com.epam.model.eat.applications;

import com.epam.model.eat.Food;
import com.epam.model.eat.PrototypeFood;

public class Mustard extends Food{

	public Mustard() {
		name = "Mustard";
		COST = 1;
	}
	
	@Override
	public PrototypeFood cloneFood() {
		Mustard mustard = new Mustard();
		mustard.setCost(COST);
		mustard.setName(name);
		return mustard;
	}

	@Override
	public String toString() {
		return "Mustard [cost=" + COST + ", name=" + name + "]";
	}
	
}
