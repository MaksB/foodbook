package com.epam.model.eat;

public interface PrototypeFood {

	public PrototypeFood cloneFood();
}
