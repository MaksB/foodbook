package com.epam.model.drink;

import com.epam.model.eat.Food;
import com.epam.model.eat.PrototypeFood;

public class Milk extends Food {

	private int fats;

	public Milk() {
		name = "Milk";
		COST = 10;
		fats = 20;
	}

	public int getFats() {
		return fats;
	}

	public void setFats(int fats) {
		this.fats = fats;
	}

	@Override
	public PrototypeFood cloneFood() {
		Milk milk = new Milk();
		milk.setCost(this.COST);
		milk.setName(this.name);
		milk.setFats(this.fats);
		return milk;
	}

	@Override
	public String toString() {
		return "Milk [fats=" + fats + ", cost=" + COST + ", name=" + name + "]";
	}

}
