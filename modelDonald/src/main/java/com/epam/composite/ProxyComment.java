package com.epam.composite;

import java.util.ArrayList;
import java.util.List;

public class ProxyComment implements CommentInterface {

	private Comment comment;
	private String[] badWords = {"Fuckwit", "Bent Richard", "Big Richard", "Blow my Richard", "Breeder", "Fairy" };

	public ProxyComment(Comment comment) {
		this.comment = comment;
	}

	public List<CommentInterface> getSubComment() {
		List<CommentInterface> proxyComments = new ArrayList<>();
		if (!comment.getSubComment().isEmpty()) {
			for (CommentInterface subComment : comment.getSubComment()) {
				proxyComments.add(new ProxyComment((Comment) subComment));
			}
		}
		return proxyComments;
	}

	@Override
	public String dispalyComment() {
		return replaceBadWords(comment.dispalyComment());
	}
	
	@Override
	public Long getRootId() {
		return comment.getRootId();
	}
	
	@Override
	public Long getId() {
		return comment.getId();
	}

	private String replaceBadWords(String message) {
		for (String badWord : badWords) {
			message = message.replaceAll(badWord, "****");
		}
		return message;
	}
}
