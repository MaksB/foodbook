package com.epam.composite;

import java.util.List;

public interface CommentInterface {

	public String dispalyComment();
	public Long getId();
	public List<CommentInterface> getSubComment();
	public Long getRootId();
	
}
