package com.epam.composite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Comment implements CommentInterface {

	private Long id;
	private Long rootId = 1L;
	private String msg;
	private Date dateComment;
	private List<CommentInterface> subComment = new ArrayList<>();
	private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm");

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getDateComment() {
		return dateComment;
	}

	public void setDateComment(Date dateComment) {
		this.dateComment = dateComment;
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CommentInterface> getSubComment() {
		return subComment;
	}

	public void setSubComment(List<CommentInterface> subComment) {
		this.subComment = subComment;
	}

	public Long getRootId() {
		return rootId;
	}

	public void setRootId(Long rootId) {
		this.rootId = rootId;
	}

	@Override
	public String dispalyComment() {	
		return msg + " " + formatter.format(dateComment);
	}

}
